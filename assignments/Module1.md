**Product Name**

Amazon Rekognition

**Product Link**

[Amazon Rekognition](https://aws.amazon.com/rekognition/?blog-cards.sort-by=item.additionalFields.createdDate&blog-cards.sort-order=desc)

**Product Short Description**

With Amazon Rekognition, we can identify objects, people, text, scenes, and activities in images and videos, as well as detect any inappropriate content. Amazon Rekognition also provides highly accurate facial analysis and facial search capabilities that you can use to detect, analyze, and compare faces for a wide variety of user verification, people counting, and public safety use cases.

**Product is combination of features**

1. Object detection
2. Person Detection
3. Text detection
4. Face detection and analysis
5. Face search and verification

**Product is provided by which company?**

Amazon Rekognition

-------------------------------------------------------------------------------------------------------------------------------------------------------

**Product Name**

Object Detection Guide

**Product Link**

[Object Detection Guide](https://www.fritz.ai/object-detection/)

**Product Short Description**

It  allows us to identify and locate objects in an image or video. With this kind of identification and localization, object detection can be used to count objects in a scene and determine and track their precise locations, all while accurately labeling them.

**Product is combination of features**

1. Person Detection
2. Image Recognition

**Product is provided by which company?**

FRITZ AI
